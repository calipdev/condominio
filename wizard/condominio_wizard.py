# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>)
#    Copyright (C) 2004 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
################################################################################

from openerp.osv import osv, fields
import netsvc
import datetime
from docutils.nodes import line
from openerp.tools.translate import _


class censo_orden_wizard(osv.TransientModel):
    _name = 'censo.orden.wizard'
    _columns = {
        'period_id': fields.many2one('account.period', 'Periodo',
                                     help='Selecciona el Periodo ',
                                     required=True),
    }

    def default_get(self, cr, uid, fields, context=None):
        """
        This function load in the wizard,
        the current period
        """
        data = super(censo_orden_wizard, self).default_get(cr, uid,
                                                                   fields, context=context)
        time_now = datetime.date.today()
        period_id = self.pool.get('account.period').search(cr, uid,
                                                           [('date_start', '<=', time_now),
                                                            ('date_stop', '>=', time_now),])
        if period_id:
            data.update({'period_id': period_id[0]})
        return data
    
   
    
    def generar_orden(self, cr, uid, ids, fields, context=None):
        res = self.read(cr, uid, ids, context=context)[0]
        period_id = res["period_id"]        
        #seleccionar todos los id de los condominio_censo con estado "proceso"        
        #crear una factura con condominio_censo_id = a cada id previamente seleccionado         
        #crear los lineas a la factura recien creada
        
        query_sql ="""select cs.id,cc.partner_id,cs.referencia 
                    from condominio_censo cs, condominio_condomino cc  
                    where cs.state='proceso'
                    and cs.condomino_id = cc.id
                    """
        cr.execute(query_sql)        
        lines = cr.dictfetchall()
        
        if not lines:
            raise osv.except_osv(
                _('Error'),
                _('Not found Journal Entries for this Period.\n'
                  'Choose another Period!')
            )
        date_invoice = datetime.date.today()
        
        for censo in lines:
            query_sql = """
                select censo_id from censo_periodo_rel 
                where period_id = %s and censo_id = %s""" %(period_id[0],censo['id'])
            cr.execute(query_sql)  
            lines_censos_periodos = cr.dictfetchall()
            check_censo = False
            for censo_periodo in lines_censos_periodos:
                if censo['id'] == censo_periodo['censo_id']:
                    check_censo = True
                    break
            if not check_censo:
                #crea el folio 
                order_id = self.generar_condominio_folio(cr, uid, ids,censo['id'],censo['partner_id'],censo['referencia'],date_invoice)
                #inserto en la tabla many2many para tener control de los censos ya procesados
                cr.execute('insert into censo_periodo_rel (censo_id,period_id) values(%s,%s)',(censo['id'],period_id[0]))
            check_censo = False
    
    def actualizar_orden(self, cr, uid, ids, fields, context=None):
        res = self.read(cr, uid, ids, context=context)[0]
        period_id = res["period_id"]        
        #seleccionar todos los id de los condominio_censo con estado "proceso"        
        #crear una factura con condominio_censo_id = a cada id previamente seleccionado         
        #crear los lineas a la factura recien creada
        
        query_sql ="select id from condominio_censo where state='proceso'"
        cr.execute(query_sql)        
        lines = cr.dictfetchall()
        
        if not lines:
            raise osv.except_osv(
                _('Error'),
                _('Not found Journal Entries for this Period.\n'
                  'Choose another Period!')
            )  
        
        for censo in lines: 
            #checo si ya se proceso el censo en el periodo a consultar
            query_sql = """
                select censo_id from censo_periodo_rel 
                where period_id = %s and censo_id = %s""" %(period_id[0],censo['id'])
            cr.execute(query_sql)  
            lines_censos_periodos = cr.dictfetchall()
            check_censo = False
            """for censo_periodo in lines_censos_periodos:
                if censo['id'] == censo_periodo['censo_id']:
                    check_censo = True
                    break
            """
            if not check_censo:
                #se confirma la orden 
                #crear la factura para ese censo
                
                #obtengo la fecha de los periodos para poder buscar las ordenes generadas en ese periodo       
                query_sql="select date_start, date_stop from account_period where id=%s"%(period_id[0])
                cr.execute(query_sql)        
                lines_periodo = cr.dictfetchall() 
                
                query_sql = "select so.id as order_id from sale_order so, condominio_censo cc where date_order >=  '%s' and date_order <= '%s' and so.state = 'draft' and so.condominio_censo_id = %s" %(str(lines_periodo[0]['date_start']), str(lines_periodo[0]['date_stop']),censo['id'])
                cr.execute(query_sql)
                lines_sale_order=cr.dictfetchall() 
                    
                for order in lines_sale_order:
                    order_id = order["order_id"]
                    wf_service = netsvc.LocalService('workflow')
                    #se confirma la orden 
                    wf_service.trg_validate(uid, 'sale.order', order_id, 'order_confirm', cr)
                    #se crea la factura
                    wf_service.trg_validate(uid, 'sale.order', order_id, 'manual_invoice', cr)
                    #se abre la factura
                    saleorder = self.pool.get('sale.order').browse(cr, uid, order_id)
                    inv_ids = self.pool.get('account.invoice').search(cr, uid, [('origin','=',saleorder.name)])
                    wf_service.trg_validate(uid, 'account.invoice',inv_ids[0], 'invoice_open', cr)
                    #se setea la factura al censo
                    self.pool.get('account.invoice').write(cr, uid, [inv_ids[0]], {'condominio_censo_id': censo['id']})                    
            check_censo = False
    
    def generar_condominio_folio(self,cr, uid, ids, condominio_censo_id, partner_id, referencia, date_order):        
        #Generar la Orden de Venta-Cobro para un condominio
        order={
           'shop_id': 1, 
           'state': 'draft',
           'date_order': date_order,
           'user_id': uid,
           'partner_id': partner_id,
           'partner_invoice_id': partner_id,
           'partner_order_id': partner_id,
           'partner_shipping_id': partner_id,
           'picking_policy': 'direct',
           'order_policy': 'manual',
           'pricelist_id': 1, 
           'note': 'Orden correspondiente al censo: '+str(condominio_censo_id),
           'client_order_ref': referencia,
           'condominio_censo_id': condominio_censo_id,
        }
        order_id = self.pool.get('sale.order').create(cr, uid, order,context=None)
        
        #crear las la linea de venta de la orden recien creada, se le pasa el order_id de la orden     
        self.generar_service_line(cr, uid, ids,1,1,order_id, condominio_censo_id)
        return order_id        
    
    def generar_service_line(self,cr, uid, ids,product_uom_qty,product_uos_qty,order_id, condominio_censo_id):        
        #recorrer los productos que sean de tipo servicio de condominio y generar una orden x cada uno
        query_sql = "select id from product_product where isservice = true"
        cr.execute(query_sql)        
        lines_product = cr.dictfetchall()  
        #Retorna los servicios a Exluir en la Linea de Venta del Condomino.
        query_sql = "select service_id from condominio_censo_servicio where condominio_censo_id = %s" %(condominio_censo_id)  
        cr.execute(query_sql)        
        lines_no_servicio = cr.dictfetchall()
          
        fields = ['name', 'uom_id','default_code','list_price','taxes_id'] #fields to read
        #Generar las lineas de la orden de venta para cada producto
        for product_id in lines_product:  
            product_fields = self.pool.get('product.product').read(cr, uid, product_id['id'],fields,context=None)
            #checar servicios a no incluir en la Orden del Condomino
            temp_serv = False
            for v in lines_no_servicio:
                if product_id['id'] == v['service_id']:
                    temp_serv = True
                    break
            if temp_serv == False:
                order_data = {
                              'name'                 : product_fields['name'],
                              'product_id':  product_id['id'],
                              'product_uom_qty': product_uom_qty,
                              'product_uom'     : product_fields['uom_id'][0],
                              'product_uos_qty': product_uos_qty,
                              'price_unit' : product_fields['list_price'],
                              'type'                   : 'make_to_stock',
                              'default_code'      : product_fields['default_code'],
                              'order_id'             :  order_id,
                            }
                service_line_id = self.pool.get('sale.order.line').create(cr, uid, order_data,context=None)
    
    def _dias_sin_pago(self, cr, uid, ids, field_name, arg, context=None):
        """checar dias de morocidad teniendo como referencia
        el dia establecido como limite en el Condominio
        """        
        #obtener el dia de cobro impuesto por la asamblea
        company_obj = self.pool.get('res.company')
        company_id = company_obj.search(cr, uid, [('partner_id','=',1)])
        empresa = self.pool.get('res.company').browse (cr, uid, company_id, context)
        dia_cobro = empresa[0].dia_cobro
        #iterar x los censos y checar el que tenga la ultima orden de cobro sin pagar 
        sale_order_day = datetime.datetime.now().day
        dias_vencido = sale_order_day-dia_cobro
        return dias_vencido 
            
    def generar_ordenes_recargo(self, cr, uid, ids, fields, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''
        #obtener el dia de cobro impuesto por la asamblea
        company_obj = self.pool.get('res.company')
        company_id = company_obj.search(cr, uid, [('partner_id','=',1)])
        empresa = self.pool.get('res.company').browse (cr, uid, company_id, context)
        monto_cobro_1 = empresa[0].monto_cobro_1
        monto_cobro_2 = empresa[0].monto_cobro_2
        #contar dias de morocidad, dependiendo de los dias se realiza el monto del recargo
        dias_vencido = self._dias_sin_pago(cr, uid, ids, field_name=None, arg=None, context=None)
        monto = 50
        #variable para saber que recargo generar 1 o 2 
        tipo_recargo = 1
        
        if dias_vencido < 5:
            raise osv.except_osv(
                _('Error'),
                _('No se han cumplido 5 dias o mas de morocidad.\n'
                  'No se pude realizar Recargos!')
            )        
        if dias_vencido < 10:
            monto = monto_cobro_1 
        if dias_vencido >10:
            tipo_recargo = 2
            monto = monto_cobro_2
             
        #obtiene el periodo desde el wizard
        res = self.read(cr, uid, ids, context=context)[0]
        period_id = res["period_id"]
        
        #obtengo la fecha de los periodos para poder buscar las ordenes generadas en ese periodo       
        query_sql="select date_start, date_stop from account_period where id=%s"%(period_id[0])
        cr.execute(query_sql)        
        lines_periodo = cr.dictfetchall()  
               
        #las facturas de los censos morosos que no se han procesados en este periodo
        query_sql="""select ai.id as factura, ai.account_id as cuenta, cc.id as censo from account_invoice ai, condominio_censo cc 
            where ai.date_due >= '%s' and ai.date_due <= '%s' and ai.state <> 'paid' 
            and ai.condominio_censo_id is not null and cc.state='proceso' and ai.condominio_censo_id = cc.id 
            and cc.id not in (select condominio_censo_id from condominio_censo_moroso where period_id = %s and recargo = %s)"""
            %(str(lines_periodo[0]['date_start']), str(lines_periodo[0]['date_stop']), str(period_id[0]), str(tipo_recargo))
        cr.execute(query_sql)        
        lines = cr.dictfetchall()
        #utilizo esta variable para cuando se cree el recargo, borrar la relacion
        #del censo con la factura vieja
        facturas_viejas = []
        if lines:
            #generan ordenes de cobros para cada censo moroso
            #obtengo el producto de recargo
            recargo_id = self.pool.get('product.product').search(cr, uid, [('isrecargo','=',True)])            
            fields = ['name', 'uom_id','default_code','list_price','taxes_id'] #fields to read
            product_fields = self.pool.get('product.product').read(cr, uid, recargo_id[0],fields,context=None)
            
            #paso 1 cancelo las facturas
            for factura in lines:
                facturas_viejas.append(factura['factura'])
                #cancelar la factura
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid,'account.invoice',factura['factura'],'invoice_cancel',cr)
                wf_service.trg_delete(uid, 'account.invoice', factura['factura'], cr)
                                
        #ya cancelada las facturas, cancelo las ordenes de los censos morosos que no se han procesados en este periodo
        query_sql="""select so.id as orden, cc.id as censo from sale_order so, condominio_censo cc 
            where so.date_order >= '%s' 
            and so.date_order <= '%s' 
            and so.state = 'invoice_except' and so.condominio_censo_id is not null and cc.state='proceso' 
            and so.condominio_censo_id = cc.id and cc.id not in (select condominio_censo_id from condominio_censo_moroso where period_id = %s 
            and recargo = %s")""" % (str(lines_periodo[0]['date_start']), str(lines_periodo[0]['date_stop']), str(period_id[0]), str(tipo_recargo))
        cr.execute(query_sql)        
        lines = cr.dictfetchall()
        if lines:            
            for orden in lines:
                order_id = orden["orden"]
                
                sale_obj = self.pool.get('sale.order')
                sale = sale_obj.browse(cr, uid, order_id, context=context)
                
                #no funciona la llamada a action_cancel
                #wf_service = netsvc.LocalService('workflow')
                #wf_service.trg_validate(uid, 'sale.order', order_id, 'action_cancel', cr)
                #wf_service.trg_delete(uid, 'sale.order', order_id, cr)
                
                #copie el codigo original de action_cancel del sale.order aqui
                #cancelo la orden
                wf_service = netsvc.LocalService("workflow")               
                sale_order_line_obj = self.pool.get('sale.order.line')
                if sale:
                    for inv in sale.invoice_ids:
                        if inv.state not in ('draft', 'cancel'):
                            raise osv.except_osv(
                                _('Cannot cancel this sales order!'),
                                _('First cancel all invoices attached to this sales order.'))
                    #for r in sale_obj.read(cr, uid, order_id, ['invoice_ids']):
                    #    for inv in r['invoice_ids']:
                    #        wf_service.trg_validate(uid, 'account.invoice', inv, 'invoice_cancel', cr)
                    sale_order_line_obj.write(cr, uid, [l.id for l in  sale.order_line],
                            {'state': 'cancel'})
                sale_obj.write(cr, uid, order_id, {'state': 'cancel'})
                
                #creo una nueva orden basada en la cancelada
                #para agregar la linea de recargo
                new_order_id = sale_obj.copy(cr, uid, order_id, default=None, context=None)
                
                #obtengo el producto de recargo
                recargo_id = self.pool.get('product.product').search(cr, uid, [('isrecargo','=',True)])            
                fields = ['name', 'uom_id','default_code','list_price','taxes_id'] #fields to read
                product_fields = self.pool.get('product.product').read(cr, uid, recargo_id[0],fields,context=None)
                #creo la linea de recargo
                recargo_data = {
                              'name'                 : product_fields['name'],
                              'product_id':  recargo_id[0],
                              'product_uom_qty': 1,
                              'product_uom'     : product_fields['uom_id'][0],
                              'product_uos_qty': 1,
                              'price_unit' : monto,
                              'type'                   : 'make_to_stock',
                              'default_code'      : product_fields['default_code'],
                              'order_id'             :  new_order_id,
                            }
                service_line_id = self.pool.get('sale.order.line').create(cr, uid, recargo_data,context=None)
                
                #confirmo el pedido nuevo y le creo la factura
                #se crea la factura
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid, 'sale.order', new_order_id, 'order_confirm', cr)
                wf_service.trg_validate(uid, 'sale.order', new_order_id, 'manual_invoice', cr)
                
                #se abre la factura
                sale_order_new = self.pool.get('sale.order').browse(cr, uid, new_order_id)
                inv_ids = self.pool.get('account.invoice').search(cr, uid, [('origin','=',sale_order_new.name)])
                wf_service.trg_validate(uid, 'account.invoice',inv_ids[0], 'invoice_open', cr)
                
                #se setea la factura al censo
                self.pool.get('account.invoice').write(cr, uid, [inv_ids[0]], {'condominio_censo_id': orden['censo']})
                
                #elimina la relacion de la factura y censo viejos con el censo
                self.pool.get('sale.order').write(cr, uid, [order_id], {'condominio_censo_id': None})
                self.pool.get('account.invoice').write(cr, uid, facturas_viejas, {'condominio_censo_id': None})
                
                #actualizo los censos ya procesados como morosos segun el recargo
                cr.execute('insert into condominio_censo_moroso (condominio_censo_id,period_id,recargo) values(%s,%s,%s)',(orden['censo'],period_id[0],tipo_recargo))
    
    def print_recibos(self, cr, uid, ids, fields, context=None):
        '''
        This function prints the sales order and mark it as sent, so that we can see more easily the next step of the workflow
        '''     
        res = self.read(cr, uid, ids, context=context)[0]
        period_id = res["period_id"]
        
        query_sql="select date_start, date_stop from account_period where id=%s"%(period_id[0])
        cr.execute(query_sql)        
        lines_periodo = cr.dictfetchall()    
             
        
        query_sql ="select id from condominio_censo where state='proceso'"
        cr.execute(query_sql)        
        lines = cr.dictfetchall()
        
        if not lines:
            raise osv.except_osv(
                _('Error'),
                _('Not found Journal Entries for this Period.\n'
                  'Choose another Period!')
            )        
        
        ids_temp = []
        for censo in lines:             
            query_sql = "select so.id  from sale_order so, condominio_censo cc where date_order >=  '"+str(lines_periodo[0]['date_start'])+"' and date_order <= '"+str(lines_periodo[0]['date_stop'])+"' and so.condominio_censo_id = %s"%(censo['id'])
            cr.execute(query_sql)
            lines_sale_order=cr.dictfetchall()           
            
            for so in lines_sale_order:
                ids_temp.append(so['id'])
                break
        
        datas = {
            'model': 'sale.order',                     
            'ids': ids_temp,
            'form': self.read(cr, uid, ids, context=context),
        }
        return {'type': 'ir.actions.report.xml', 'report_name': 'sale.order.recibos', 'datas': datas}
                    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: