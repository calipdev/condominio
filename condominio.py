# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2013 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import datetime

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from _ast import Store

class condominio_censo(osv.Model):
    _name = "condominio.censo"
    _rec_name = "id"
    
    _columns = {
        'id': fields.integer('Numero de control', readonly=True),        
        'create_date': fields.datetime('Fecha emision', readonly=True),
        'levantamiento_date': fields.datetime('Fecha levantamiento'),
        'entrega_inmueble_date': fields.datetime('Fecha entrega inmueble'),
        'descripcion': fields.text('Descripcion'),
        'nota_recibo_1': fields.text('Nota de Recibo 1'),        
        'referencia': fields.char('Referencia',required=True),
        'departamento_id': fields.many2one('condominio.room','Departamento',required=True),
        'torre_id': fields.related(
            'departamento_id', 'torre_id', 
            type='many2one', relation='condominio.torre',
            string='Torre', readonly=True),
        'status_depa': fields.related(
            'departamento_id', 'status', type='char',             
            string='Estado', readonly=True),
        'habitado_por': fields.related(
            'departamento_id', 'habitado', type='char',             
            string='Habitado Por', readonly=True),
        'cond_censo_serv_ids': fields.one2many('condominio.censo.servicio','condominio_censo_id','Servicios'),#Servicios Exluidos        
        'cond_censo_mascota_ids': fields.one2many('condominio.censo.mascota','condominio_censo_id','Mascotas'),#Mascotas       
        'sale_order_ids': fields.one2many('sale.order','condominio_censo_id','Folios'),         
        'account_invoice_ids': fields.one2many('account.invoice','condominio_censo_id','Facturas'),      
        'condomino_id': fields.many2one( 'condominio.condomino', 'Propietario',required=True), 
        'periodo_ids': fields.many2many('account.period','censo_periodo_rel','censo_id','period_id','Periodos'),
        'state': fields.selection([
            ('proceso','En Proceso'),
            ('finalizado','Finalizado'),
            ],'Estado', select=True, readonly=True, track_visibility='onchange',
            help=' * Estado \'En Proceso\' se establece cuando en Censo esta Activo. \
            \n* Estado \'Finalizado\' se establece cuando se Finaliza el Censo.'),
    }
    
    def onchange_departamento_id(self, cr, uid, ids,departamento_id, context={}):
        res={}        
        query_sql = """select distinct cr.condominio_condomino_id from condominio_condomino_room cr, condominio_condomino cc 
        where cc.id = condominio_condomino_id and cr.condominio_room_id = %d and cc.clasificacion = 'propietario'"""%(departamento_id)
        cr.execute(query_sql)    
        result = [x[0] for x in cr.fetchall()]        
        res['value'] = {'condomino_id':result}
        return res  

class condominio_torre(osv.Model):
    _name = "condominio.torre"
    _description = "Torre"
    _columns = { 
        'name': fields.char('Nombre', size=64, required=True, select=True),
        'descripcion': fields.text('Descripcion'),

    }

class product_category(osv.Model):
    _inherit = "product.category"
    _columns = {
        'isroomtype': fields.boolean('Is Room Type'),
        'isservicetype': fields.boolean('Is Service Type'),
    }

class product_product(osv.Model):
    _inherit = "product.product"
    _columns = {
        'isroom': fields.boolean('Is Room'),
        'iscategid': fields.boolean('Is categ id'),
        'isservice': fields.boolean('Is Service id'),
        'isrecargo': fields.boolean('Es Recargo'),
    }

class condominio_room(osv.Model):
    _name = 'condominio.room'
    _inherits = {'product.product': 'product_id'}
    _description = 'Departamentos'
    _columns = {
        'id': fields.integer('Departmaneto Id'),        
        'product_id': fields.many2one('product.product', 'Product_id', required=True, ondelete='cascade'),
        'torre_id': fields.many2one('condominio.torre', 'Torre', required=True),
        'max_adult': fields.integer('Max Adult'),
        'max_child': fields.integer('Max Child'),
        'status': fields.selection([('disponible', 'Disponible'),('ocupado','Ocupado'),('litigio','Litigio')], 'Status'),
        'habitado': fields.selection([('propietario', 'Propietario'),('inquilino','Inquilino'),('familiar','Familiar')], 'Habitado Por'),   
        'condominio_condomino_room_ids': fields.one2many('condominio.condomino.room','condominio_room_id','Departamentos'),#Departamentos       
    }
    _defaults = {
        'isroom': 1,
        'status': 'disponible',
        'sale_ok': 0,
    }

class condominio_service_type(osv.Model):
    _name = "condominio.service.type"
    _inherits = {'product.category':'ser_id'}
    _description = "Service Type"
    _columns = { 
        'ser_id': fields.many2one('product.category', 'Category', required=True, select=True, ondelete='cascade'),
    }
    _defaults = {
        'isservicetype': 1,
    }

class condominio_services(osv.Model):
    _name = 'condominio.services'
    _description = 'condominio Services and its charges'
    _inherits = {'product.product':'service_id'}
    _columns = {
        'service_id': fields.many2one('product.product', 'Service id', required=True, ondelete='cascade'),
    }
    _defaults = {
        'isservice': 1,
        'type':'service',
    }

class condominio_censo_servicio(osv.Model):
    _name = "condominio.censo.servicio"    
    _columns = {        
        'service_id': fields.many2one('product.product', 'Service id', required=True),
        'condominio_censo_id': fields.many2one('condominio.censo', 'Censo de Condominio'),     
    }

class condominio_censo_mascota(osv.Model):
    _name = "condominio.censo.mascota"    
    _columns = {   
        'tipo': fields.char("Tipo"),
        'raza': fields.char("Raza"),
        'cantidad': fields.integer("Cantidad"),     
        'condominio_censo_id': fields.many2one('condominio.censo', 'Censo de Condominio'),     
    }

class res_company(osv.Model):
    _inherit = 'res.company'
    _columns = {
        'dia_cobro': fields.integer('Dia de Cobro de Servicios', help="Establece el dia para Cobrar los Servicios de Mantenimiento a los Condominos"),
        'nota_recibo_1': fields.text('Nota de Recibo 1',required=True),
        'nota_recibo_2': fields.text('Nota de Recibo 2'),
        'monto_cobro_1': fields.float('Monto Primer Recargo', help="Establece el monto a Cobrar con 5 dias de retraso"),
        'monto_cobro_2': fields.float('Monto Segundo Recargo', help="Establece el monto a Cobrar con 10 dias de retraso"),
    }
    
class condominio_cajon(osv.Model):
    _name = 'condominio.cajon'
    _description = 'Cajones de Estacionamiento'    
    _columns = {
        'name': fields.char('Nombre'),
        'numero': fields.char('Numero', required=True),
        'seccion': fields.char('Seccion', required=True),
        'status': fields.selection([('prestado', 'Prestado'),('rentado','Rentado'),('vacio','Vacio'),('ocupado','Ocupado por Propietario')], 'Status',required=True),
        'departamento_id': fields.many2one('condominio.room', 'Departamento', required=True, ondelete='cascade'),   
        'departamento_prest_id': fields.many2one( 'condominio.room', 'Prestado/Rentado a'), 
        'fin_prestamo_date': fields.datetime('Fecha Fin de Prestamo/Renta'),
    }
    
    def create(self, cr, uid, vals, context=None):
        vals['name'] = str(vals.get('seccion')) +'-'+ str(vals.get('numero'))
        return super(condominio_cajon,self).create(cr, uid, vals, context=context)
    
    _sql_constraints = [('name_unique', 'unique(name)', 'Ya Existe un Cajon con ese Numero en esa Seccion')]
    
class res_partner(osv.Model):
    _inherit = 'res.partner'
    _columns = {
                'iscondomino': fields.boolean('Es Condomino'),   
    }

class condominio_condomino(osv.Model):
    _inherits = {'res.partner':'partner_id'}
    _name = 'condominio.condomino'
    _description = 'Condominos'
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner id', required=True, ondelete='cascade'),
        'clasificacion': fields.selection([('propietario', 'Propietario'),('familiar','Familiar'),('inquilino','Inquilino'),('poseedor','Poseedor')], 'Clasificacion',required=True),
        'condominio_condomino_room_ids': fields.one2many('condominio.condomino.room','condominio_condomino_id','Condominos'),#Departamentos       
    }
    _defaults = {
            'iscondomino': True,
        }
class condominio_censo_moroso(osv.Model):  
    """Tabla para llevar el control de las recargas a los cenos
    en un determinado periodo
    """  
    _name = 'condominio.censo.moroso'
    _description = 'Condominos censo periodo morosos'
    _columns = {
        'recargo': fields.integer('Recargo'),
        'period_id': fields.many2one('account.period','Periodos'),
        'condominio_censo_id': fields.many2one('condominio.censo','Censos'),
    }

class condominio_condomino_room(osv.Model):
    _name = "condominio.condomino.room"    
    _columns = {        
        'condominio_condomino_id': fields.many2one('condominio.condomino', 'Condomino'),
        'condominio_room_id': fields.many2one('condominio.room', 'Departamento'),     
    }

class account_invoice(osv.Model):
    _inherit = 'account.invoice'
    _columns = {
        'condominio_censo_id': fields.many2one('condominio.censo', string="Censo", readonly=True),
        }
    
class sale_order(osv.Model):
    _inherit = 'sale.order'
    _columns = {
        'condominio_censo_id': fields.many2one('condominio.censo', string="Censo", readonly=True),
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
